# Computational Thinking
**Computational thinking**

*Computational thinking is an approach to solving problems using concepts and ideas*

*from computer science, and expressing solutions to those problems so that they can be run on a*

*computer. Computational thinking involves breaking down a problem in smaller parts, looking*

*for patterns in those sub problems, figuring out what information is needed, and developing a*

*step-by- step solution*

*There are four what we call pillars to computational thinking:*

\1. *decomposition*

\2. *pattern recognition*

\3. *abstraction*

\4. *algorithms*

**decomposition**

• *Decomposition means breaking a complex problem into more manageable sub-problems*

• *Putting the solutions to the sub problems together gives a solution to the original,*

*complex problem*

**Pattern recognition**

•

*Pattern recognition means finding similarities or shared characteristics within or*

*between problems*

•

*The same solution can be used for each occurrence of the pattern.*

**abstraction**

•

*In data representation and abstraction, we determine what characteristics of the*

*problem are important and filter out those that are not*

•

*Use these to create a representation of what we are trying to solve*

**algorithms**

• *An algorithm is a set of step-by step instructions of how to solve a problem*

• *Identifies what is to be done (the instructions), and the order in which they should be done*



• *Can be described in English, as a flowchart, or by using pseudo-code.*
